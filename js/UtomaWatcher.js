'use strict'

class UtomaWatcher {
	constructor (utoma, template, update_interval=100) {
		this.update_interval = update_interval;
		this.utoma = utoma;
		this.template = template;
		this.interval_handler = (utoma != null)? window.setInterval (() => this.update_template (), update_interval): null;
		this.monitor = this.template.querySelector ("#monitor");
		this.eyes = new Array (9);
		this.neurons = new Array (6);
		for (let i=0; i<9; i++) {
			this.eyes[i] = this.monitor.contentDocument.getElementById("eye_"+i)
		}
		this.altitude_in = this.monitor.contentDocument.getElementById("altitude_in");
		this.feedback_in_1 = this.monitor.contentDocument.getElementById("feedback_in_1");
		this.feedback_in_2 = this.monitor.contentDocument.getElementById("feedback_in_2");
		this.feedback_in_3 = this.monitor.contentDocument.getElementById("feedback_in_3");
		this.speed_in = this.monitor.contentDocument.getElementById("speed_in");
		this.antenna_slices = new Array (6);
		for (let i=0; i<6; i++) {
			this.antenna_slices[i] = this.monitor.contentDocument.getElementById("antenna_"+i);
		}
		this.mark_active = this.monitor.contentDocument.getElementById("mark_active");
		this.mark_color = this.monitor.contentDocument.getElementById("mark_color");
		this.altitude_up = this.monitor.contentDocument.getElementById("altitude_up");
		this.altitude_down = this.monitor.contentDocument.getElementById("altitude_down");
		this.rotation_left = this.monitor.contentDocument.getElementById("rotation_left");
		this.rotation_right = this.monitor.contentDocument.getElementById("rotation_right");
		this.speed_out= this.monitor.contentDocument.getElementById("speed_out");
		this.feedback_out_1 = this.monitor.contentDocument.getElementById("feedback_out_1");
		this.feedback_out_2 = this.monitor.contentDocument.getElementById("feedback_out_2");
		this.feedback_out_3 = this.monitor.contentDocument.getElementById("feedback_out_3");
		for (let i=0; i<6; i++) {
			this.neurons[i] = this.monitor.contentDocument.getElementById("neuron_"+i)
		}
	}

	watch (utoma) {
		if (this.utoma != null) {
			this.utoma.sprite.classList.remove ("watching");
		}
		this.utoma = utoma;
		if (this.interval_handler != null) {
			window.clearInterval (this.interval_handler);
			this.interval_handler = null;
		}
		if (utoma != null) {
			utoma.sprite.classList.add("watching");
			window.setInterval (() => this.update_template (), this.update_interval)
		}
	}

	unwatch () {
		this.watch (null);
	}

	update_template () {
		if (this.utoma != null) {
			for (let i=0; i < 9; i++) {
				let r = this.utoma.get_input ('EYE_'+i+'_R');
				let g = this.utoma.get_input ('EYE_'+i+'_G');
				let b = this.utoma.get_input ('EYE_'+i+'_B');
				this.eyes[i].style.fill= 'rgb('+ r +','+ g +','+ b +')';
			}

			for (let i=0; i < 6; i++) {
				var neuron_value = this.utoma.neural.computed_layers[1][i];
				this.neurons[i].style.fill = this.get_red_to_green_interpolation(neuron_value);
			}

			this.speed_in.style.fill = this.get_white_to_green_interpolation (this.utoma.get_input('SPEED')/UTOMA_MAX_SPEED);
			this.altitude_in.style.fill = this.get_white_to_green_interpolation (this.utoma.get_input('ALTITUDE'));
			for (let i=1; i < 4; i++) {
				var in_value = this.utoma.get_input('FEEDBACK_IN_'+i);
				this["feedback_in_"+i].style.fill = this.get_red_to_green_interpolation (in_value);

				var out_value = this.utoma.get_output('FEEDBACK_OUT_'+i);
				this["feedback_out_"+i].style.fill = this.get_red_to_green_interpolation (out_value);
			}

			var altitude_out = this.utoma.get_output('ALTITUDE')
			this.altitude_up = this.get_white_to_green_interpolation (Math.max (0, altitude_out));
			this.altitude_down = this.get_white_to_green_interpolation (Math.max (0, -altitude_out));

			var rotation_out = this.utoma.get_output('ROTATION');
			this.rotation_right.style.fill = this.get_white_to_green_interpolation (Math.max (0, rotation_out));
			this.rotation_left.style.fill = this.get_white_to_green_interpolation (Math.max (0, -rotation_out));


			this.speed_out.style.fill = this.get_white_to_green_interpolation (this.utoma.get_output('SPEED'));
			this.mark_active.style.fill = this.get_white_to_green_interpolation (this.utoma.get_output('MARK_ACTIVE'));
			this.mark_color.style.fill = this.get_antenna_color (this.utoma.get_output('MARK_ACTIVE'));


		} else {
			this.unwatch();
		}
	}

	get_red_to_green_interpolation(x) {
		if (x < 0) {
			return this.get_white_to_red_interpolation(-x);
		} else {
			return this.get_white_to_green_interpolation(x);
		}
	}

	get_white_to_green_interpolation (x) {
        var r = Math.round(Math.max(0,255-4*x*100));
        var g = Math.round(Math.min(255, 384-2*x*100));
        var b = Math.round(Math.max(0,255-4*x*100));

		return 'rgb('+ r +','+ g +','+ b +')';
	}

	get_white_to_red_interpolation (x) {
        var r = Math.round(Math.min(255, 384-2*x*100));
        var g = Math.round(Math.max(0,255-4*x*100));
        var b = Math.round(Math.max(0,255-4*x*100));

		return 'rgb('+ r +','+ g +','+ b +')';
	}

	notify_antenna (antenna_number, color, amplitude) {
		let r = (color < 0)? 255: Math.round((1-color)*255);
		let g = (color > 0)? 255: Math.round((color+1)*255);
		let b = 0;
		this.antenna_slices[antenna_number].style.fill= 'rgb('+ r +','+ g +','+ b +')';
		window.setTimeout (() => {
			this.antenna_slices[antenna_number].style.fill= 'rgb(255,255,255)';
		}, 200);
	}

	get_antenna_color(color) {
		let r = (color < 0)? 255: Math.round((1-color)*255);
		let g = (color > 0)? 255: Math.round((color+1)*255);
		let b = 0;
		return 'rgb('+ r +','+ g +','+ b +')';
	}
}
