'use strict'

var simulation_board;
var watcher;
var configs = [
	{
		img: "img/prueba.png",
		num_bees: 40,
		targets: [{x: 0.5, y: 0.5}]
	},
	{
		img: "img/jim_carrey.jpg",
		num_bees: 60,
		targets: [{x: 0.32, y: 0.4},{x: 0.48, y: 0.405}]
	},
	{
		img: "img/jupiter.jpg",
		num_bees: 80,
		targets: [{x: 0.19, y: 0.59}]
	},
	{
		img: "img/montana.jpg",
		num_bees: 80,
		targets: [{x: 0.62, y: 0.23},{x: 0.88, y:0.19}]
	}
];

var current_config = configs[0];

function init () {
	var watcher_template = document.querySelector ('.utoma_watcher');
	watcher = new UtomaWatcher (null, watcher_template);
	var board = new Board ('.board', current_config.img, current_config.num_bees, watcher);
	simulation_board = board;
	init_controls ();
	window.setTimeout (init_continue, 1000);
	console.log = (text) => {
		document.querySelector(".log").textContent += text + "\n";
	}

	document.getElementById("section_board").classList.remove ("w3-hide");
	document.getElementById("section_controls").classList.remove ("w3-hide");
}

function init_continue () {
	var targets = current_config.targets;
	targets.forEach ((target) => {
		simulation_board.evolutionary.add_target (target);
		simulation_board.draw_target (target);
	});
	simulation_board.bees.forEach ((bee) => bee.set_watcher (watcher));

	document.getElementById("config_dialog").classList.add("w3-hide")
}

function simulation_step() {
	simulation_board.step(true);
}

function init_controls () {
	var control_vars =  [
		"EVOLUTIONARY_SENSE_INTERVAL", 
		"EVOLUTIONARY_OFFSPRING_DURATION", 
		"EVOLUTIONARY_REPLACE_RATIO", 
		"EVOLUTIONARY_CROSS_POINTS", 
		"MUTATION_PROBABILITY", 
		"MUTATION_AMPLITUDE", 
		"UTOMA_MIN_SPEED",
		"UTOMA_MAX_SPEED",
		"UTOMA_ROTATION_SPEED",
		"UTOMA_ACCELERATION",
		"UTOMA_UPDATE_SPRITE_INTERVAL"
	];

	control_vars.forEach ((control) => {
		let input = document.querySelector ("input[data-var="+control+"]");
		input.value = window[control];
		input.addEventListener("input", (evt) => {
			let param = evt.target.dataset['var'];
			window[param] = Number (evt.target.value);
		});
	});
}


function play_simulation () {
	simulation_board.play ();
}

function speed1x() {
	var input = document.querySelector("input.utoma-parameter[data-var=UTOMA_UPDATE_SPRITE_INTERVAL]")
	input.value=1;
	input.dispatchEvent(new Event("input"));
}

function speed10x() {
	var input = document.querySelector("input.utoma-parameter[data-var=UTOMA_UPDATE_SPRITE_INTERVAL]")
	input.value=10;
	input.dispatchEvent(new Event("input"));
}

function speed100x() {
	var input = document.querySelector("input.utoma-parameter[data-var=UTOMA_UPDATE_SPRITE_INTERVAL]")
	input.value=100;
	input.dispatchEvent(new Event("input"));
}

function pause_simulation () {
	simulation_board.pause ();
}

function toggle_config_controls () {
	var panel = document.querySelector (".controls");
	var visible = !(panel.classList.contains ("w3-hide"));
	if (visible) {
		panel.classList.add ("w3-hide");
	} else {
		panel.classList.remove ("w3-hide");
	}
}

function toggle_watcher () {
	var panel = document.querySelector (".utoma_watcher");
	var visible = !(panel.classList.contains ("w3-hide"));
	if (visible) {
		panel.classList.add ("w3-hide");
	} else {
		panel.classList.remove ("w3-hide");
	}
}

function on_select_config_change () {
	var selected_index = document.getElementById("select_config").value;
	current_config = configs[selected_index];
	document.getElementById("input_config_num_bees").value = current_config.num_bees;

}

function on_button_init_click () {
	document.getElementById("button_init").textContent = "INICIALIZANDO ..."
	document.getElementById("button_init").disabled = true;
	var selected_index = document.getElementById("select_config").value;
	current_config = configs[selected_index];
	current_config.num_bees = document.getElementById("input_config_num_bees").value;
	init()
}

