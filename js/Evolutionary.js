var EVOLUTIONARY_SENSE_INTERVAL = 1;
var EVOLUTIONARY_OFFSPRING_DURATION = 1000;
var EVOLUTIONARY_CROSS_POINTS = 2;
var EVOLUTIONARY_REPLACE_RATIO = 0.2;
var MUTATION_PROBABILITY = 0.3;
var MUTATION_AMPLITUDE = 0.05;


class Evolutionary {

	constructor (board) {
		this.board = board;
		this.targets = [];
		this.offspring_count = 0;
		this.offspring_start_time = Date.now();
	}

	add_target (target) {
		this.targets.push (target);
	}

	distance_to_closest_target (subject) {
		var min = null;
		if (this.targets.length > 0) {
			var min = Math.sqrt (Math.pow (this.targets[0].x - subject.x, 2), Math.pow (this.targets[0].y - subject.y, 2));
			for (let i=1; i<this.targets.length; i++) {
				let distance = Math.sqrt (Math.pow (this.targets[i].x - subject.x, 2), Math.pow (this.targets[i].y - subject.y, 2));
				if (distance < min) {
					min = distance;
				}
			}
		}
		return min;
	}

	sense () {
		this.board.bees.forEach((bee) => {
			if (!bee.hasOwnProperty('cummulative_distance')) {
				bee.cummulative_distance = 0;
			}
			bee.cummulative_distance += this.distance_to_closest_target (bee);
		});
	}

	recalc_fitness () {
		var global_fitness = 0;
		this.board.bees.forEach((bee) => {
			bee.fitness = -bee.cummulative_distance / bee.step_count;
			global_fitness += bee.fitness;
		});
		console.log ("global_fitness = " + global_fitness);
		return global_fitness;
	}

	get_ordered_bees () {
		var ordered = [];
		for (let i=0; i<this.board.bees.length; i++)  {
			if (this.board.bees[i].hasOwnProperty('fitness')) {
				ordered.push (this.board.bees[i]);
			}
		}
		ordered.sort ((b, a) => {
			if (a.hasOwnProperty('fitness') && (b.hasOwnProperty('fitness'))) {
				return ((a.fitness < b.fitness)? -1: ((a.fitness > b.fitness)? 1: 0))
			} else if (a.hasOwnProperty ('fitness')) {
				return 1;
			} else if (b.hasOwnProperty ('fitness')) {
				return -1;
			} else {
				return 0;
			}
		});
		return ordered;
	}

	offspring () {
		var now = Date.now();
		this.offspring_count++;
		var elapsed = (now-this.offspring_start_time) / 1000;
		console.log ("[OFFSPRING "+ this.offspring_count + "] " +this.board.step_count+" steps in "+elapsed+" seconds (" + Math.round (this.board.step_count/elapsed) +" step/second)");
		var global_fitness = this.recalc_fitness ();
		var remove_count = Math.round (EVOLUTIONARY_REPLACE_RATIO*this.board.bees.length);
		console.log ("Replace "+remove_count+" bees");
		this.remove_bad_bees (remove_count, global_fitness);
		for (let i=0; i<remove_count; i++) {
			var parents = this.select_good_bees (2, global_fitness);
			this.cross_bees (parents[0], parents[1]);
		}
		this.offspring_start_time = Date.now();
		this.respawn ();
	}

	respawn () {
		this.board.step_count = 0;
		this.board.bees.forEach ((bee) => {
			bee.stay = false;
			bee.fitness = 0;
			bee.step_count = 0;
			bee.x = Math.random () * this.board.width;
			bee.y = Math.random () * this.board.height;
			bee.z = Math.random ();
			bee.rotation = Math.random () * 2*Math.PI;
			bee.set_input ('FEEDBACK_IN_1', 0);
			bee.set_input ('FEEDBACK_IN_2', 0);
			bee.set_input ('FEEDBACK_IN_3', 0);
		});
	}

	remove_bad_bees (count, global_fitness) {
		var bad_bees = this.select_bad_bees (count, global_fitness);
		for (let i=0; i<count; i++) {
			this.board.remove_bee (bad_bees [i]);
		}
	}

	select_good_bees (count, global_fitness) {
		var selected = [];
		var ord = this.get_ordered_bees()
		var wrost = ord[ord.length-1].fitness;
		var index = [];
		var index_sum = 0;
		for (let i=0; i<ord.length; i++) {
			index[i] = Math.abs(wrost) - Math.abs (ord[i].fitness);
			index_sum += index[i];
		}
		while (selected.length < count) {
			let selection_point = Math.random() * index_sum;
			let selected_number = -1;
			let fitness_cummulative = 0;
			while (fitness_cummulative <= selection_point) {
				selected_number += 1;
				fitness_cummulative += index[selected_number];
			}
			let bee = ord[selected_number];
			if (selected.indexOf (bee) == -1) {
				selected.push (bee);
			}
		}
		return selected;
	}

	select_good_bees_old (count) {
		var selected = [];
		var ord = this.get_ordered_bees();
		for (let i=0; i<count; i++) {
			let bee_number = (Math.floor(1+Math.random()*ord.length/3));
			let bee = ord[bee_number];
			selected.push (bee);
			// console.log ("Abeja buena: " + bee_number);
		}
		
		return selected;
	}

	select_bad_bees (count, global_fitness) {
		var selected = [];
		var ord = this.get_ordered_bees()
		while (selected.length < count) {
			let selection_point = Math.abs (global_fitness * Math.random ());
			let selected_number = ord.length;
			let fitness_cummulative = 0;
			while (fitness_cummulative < selection_point) {
				selected_number -= 1;
				fitness_cummulative += Math.abs (ord[selected_number].fitness);
			}
			let bee = ord[selected_number];
			if (selected.indexOf (bee) == -1) {
				selected.push (bee);
			}
		}
		return selected;
	}

	cross_bees (parent_1, parent_2) {
		var genome_1 = parent_1.neural.get_genome ();
		var genome_2 = parent_2.neural.get_genome ();
		var genome_child = this.cross_genomes (genome_1, genome_2);
		var child = this.board.add_bee ();
		if (Math.random() < MUTATION_PROBABILITY) {
			this.mutate_genome (genome_child);
			child.sprite.src='img/abeja_roja.png';
		}
		child.neural.set_genome (genome_child);
	}

	cross_genomes (genome_1, genome_2) {
		var genome_a = genome_1;
		var genome_b = genome_2;
		var genome_child = [];

		if (Math.random () > 0.5) {
			genome_b = genome_1;
			genome_a = genome_2;
		}

		var cross_points = [];
		for (let i=1; i<EVOLUTIONARY_CROSS_POINTS; i++) {
			genome_child = genome_child.concat (genome_a.slice (cross_points[i-1], cross_points[i]));
			let genome_tmp = genome_a;
			genome_a = genome_b;
			genome_b = genome_tmp;
		}
		genome_child = genome_child.concat (genome_a.slice (cross_points[cross_points.length-1], cross_points.length));

		return genome_child;
	}

	mutate_genome (genome) {
		for (let i=1; i<genome.length; i++) {
			let tmp = genome[i]
			genome[i] = tmp - MUTATION_AMPLITUDE + 2* MUTATION_AMPLITUDE * Math.random ();
		}
	}

}
