'use strict'

class Board {

	constructor (dom_selector, img_path, num_bees=10, watcher=null) {
		this.canvas = null;
		this.paused = true;
		this.num_bees = num_bees;
		this.bees = [];
		this.container = document.querySelector (dom_selector);
		this.load_image (img_path);
		this.width = 1;
		this.height = 1;
		this.step_count = 0;
		this.marks = [];
		this.mark_lifetime = 20;
		this.broadcast_radius = 0.1;
		this.evolutionary = new Evolutionary (this);
		this.watcher = watcher;
		this.infiniteStep();
	}

	infiniteStep () {
		this.step (); this.step (); this.step (); this.step ();
		window.setTimeout (()=>this.infiniteStep (), 0);
	}

	step (force=false) {
		if (!this.paused || force) {
			this.bees.forEach ((bee) => bee.step())
			this.step_count++;
			while ((this.marks.length > 0) && (this.step_count - this.marks[0].creation_time > this.mark_lifetime)) {
				this.remove_first_mark ();
			}
			if (this.marks.length > 0) {
				this.marks.forEach ((mark) => {
					mark.sprite.style.opacity = mark.amplitude;
					this.broadcast_mark (mark);
				});
				this.clear_marks();
			}
			if (this.step_count % EVOLUTIONARY_SENSE_INTERVAL == 0) {
				this.evolutionary.sense ();
			}
			if (this.step_count % EVOLUTIONARY_OFFSPRING_DURATION == 0) {
				this.evolutionary.offspring ();
			}
		}
	}

	load_image (img_path) {
		var img = new Image();
		img.addEventListener ('load', (evt) => this.on_image_load (evt));
		img.src = img_path;
		img.setAttribute ('crossOrigin', '');
		this.container.style.backgroundImage = "url('"+img_path+"')"
	}

	on_image_load (evt) {
		this.img = evt.target;
		this.adjust_board (this.img);
		this.canvas = document.createElement('CANVAS');
		this.canvas_context = this.canvas.getContext ('2d');
		this.canvas.width = this.img.width;
		this.canvas.height = this.img.height;
		this.canvas_context.drawImage (this.img, 0, 0);
		this.image_data =  {'width': this.img.width, 'height': this.img.height, 'pixels': []}
		for (let x=0; x < this.img.width; x++) {
			this.image_data.pixels[x] = [];
			for (let y=0; y < this.img.height; y++) {
				this.image_data.pixels[x][y] = this.canvas_context.getImageData (x,y, 1,1).data;
			}
		}
		this.image_scale = this.container.getBoundingClientRect().height;
		for (let i=0; i<this.num_bees; i++) {
			this.add_bee ();
		}
		window.addEventListener('resize', () => this.adjust_board());
	}

	adjust_board () {
		var rect = this.container.parentNode.getBoundingClientRect();
		var img_aspect_ratio = this.img.width/this.img.height;
		var board_aspect_ratio = rect.width/rect.height;
		var new_width = rect.width;
		var new_height = rect.height;
		if (img_aspect_ratio > board_aspect_ratio) {
			new_height = rect.width/img_aspect_ratio;
		} else {
			new_width = rect.height*img_aspect_ratio;
		}
		this.width = img_aspect_ratio;
		this.container.style.width = new_width + 'px';
		this.container.style.height = new_height + 'px';
	}

	add_bee () {
		var bee = new Utoma (this.container, this.canvas_context, this, this.image_data);
		bee.set_watcher (this.watcher);
		this.bees.push (bee);
		return bee;
	}

	remove_bee (bee) {
		var index = this.bees.indexOf (bee);

		if (this.watcher.utoma == bee) {
			this.watcher.unwatch();
		}
		if (index > -1) {
			if (bee.hasOwnProperty('sprite')) {
				bee.sprite.parentNode.removeChild (bee.sprite);
			}

			this.bees.splice (index, 1);
		}
	}

	play () {
		this.paused = false;
		this.bees.forEach ((bee) => bee.play ());
	}

	pause () {
		this.paused = true;
		this.bees.forEach ((bee) => bee.pause ());
	}

	add_mark (mark) {
		mark.sprite = this.create_mark_sprite (mark);
		this.marks.push (mark);
	}


	create_mark_sprite (mark) {
		var sprite = document.createElement ("DIV");
		sprite.classList.add ("mark");
		sprite.style.left =((mark.x*this.image_scale) -12)+"px";
		sprite.style.top = ((mark.y*this.image_scale) -12)+"px";
		var r = (mark.color < 0)? 255: Math.round((1-mark.color)*255);
		var g = (mark.color > 0)? 255: Math.round((mark.color+1)*255);
		var b = 0;
		mark.rgb = 'rgb('+r+','+g+','+b+')';
		sprite.style.backgroundColor = mark.rgb
		this.container.appendChild (sprite);
		return sprite;
	}

	clear_marks () {
		while (this.marks.length > 0) {
			let mark = this.marks.shift();
			window.setTimeout (() => {this.container.removeChild (mark.sprite)}, 100);;
		}
	}

	broadcast_mark (mark) {
		this.bees.forEach ((bee)=> {
			if (
				(Math.abs (bee.x - mark.x) <= this.broadcast_radius)
				&& (Math.abs (bee.y - mark.y) <= this.broadcast_radius)
				&& (Math.sqrt (Math.pow (bee.y - mark.y, 2) + Math.pow (bee.x - bee.x, 2))  <= this.broadcast_radius)
			) {
				bee.notify_mark (mark);
			}
		});
	}

	draw_target (target) {
		var sprite = document.createElement ("DIV");
		sprite.classList.add ("target");
		sprite.style.left =((target.x*this.image_scale) -12)+"px";
		sprite.style.top = ((target.y*this.image_scale) -12)+"px";
		this.container.appendChild (sprite);
	}
}
