class Neural {
	constructor (structure) {
		//Structure: Array de enteros que especifica cuantas neuronas hay en cada capa
		this.structure = structure;
		this.create_input_layer (structure)
		this.create_weights (structure)
		this.create_output_layer (structure)
		this.computed_layers = [];
	}

	create_weights (structure) {
		this.weights = new Array (structure.length);
		this.weights[0] = [];
		// this.weights[0] no se usa, porque la capa de entrada no tiene entradas
		var num_layers = structure.length;
		for (let layer=1; layer < num_layers; layer++) {
			var num_neurons = structure[layer];
			var num_inbounds = structure[layer-1];
			this.weights[layer] = new Array (num_neurons);
			for (let neuron=0; neuron < num_neurons; neuron++) {
				this.weights[layer][neuron] = new Array (num_inbounds+1);
				for (let inbound=0; inbound <= num_inbounds; inbound++) {
					// 1 extra inbound for bias
					this.weights[layer][neuron][inbound] = 1 - 2*Math.random();
				}
			}
		}
	}

	create_input_layer (structure) {
		this.inputs = new Array (structure[0]);
		for (let i=0; i < structure[0]; i++) {
			this.inputs[i] = 0;
		}
	}

	create_output_layer (structure) {
		this.outputs = new Array (structure[structure.length -1]);
		for (let i=0; i < structure[structure.length -1]; i++) {
			this.outputs[i] = 0;
		}
	}

	get_genome () {
		var flatten = [];
		this.flat (this.weights, flatten);
		return (flatten);
	}

	flat (src, dest) {
		src.forEach ((elem) => {
			if (Array.isArray (elem)) {
				this.flat (elem, dest);	
			} else {
				dest.push (elem);
			}
		});
	}

	set_genome (genome) {
		this.unflat (genome, this.weights);
	}

	unflat (genome, weights) {
		for (let i=0; i < weights.length; i++) {
			if (Array.isArray (weights[i])) {
				this.unflat (genome, weights[i]);
			} else {
				weights[i] = genome.shift ();
			}
		}
	}

	calculate_outputs () {
		var in_data = null;
		var computed = this.inputs;
		for (let layer = 1; layer < this.weights.length; layer++) {
			in_data = computed; // Input for this layer is previously computed data
			computed = [];
			for (let neuron=0; neuron < this.structure[layer]; neuron++) {
				let sum = 0;
				let inbound = 0;
				for (inbound = 0; inbound < in_data.length; inbound++) {
					sum += in_data[inbound] * this.weights[layer][neuron][inbound];
				}
				sum += this.weights[layer][neuron][inbound];
				computed.push (this.activation_function (sum));
			}
			this.computed_layers[layer] = computed;
		}
		this.outputs = computed;
	}

	activation_function (input) {
		return Math.tanh (input); // TANH
	}
}
//Formato de weights: this.weights[layer_number][neuron_number_in_layer][inbound_number]
