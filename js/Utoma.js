'use strict'

var UTOMA_ROTATION_SPEED = 0.1;
var UTOMA_ACCELERATION = 0.1;
var UTOMA_UPDATE_SPRITE_INTERVAL = 1;
var UTOMA_MIN_SPEED = 0.001;
var UTOMA_MAX_SPEED = 0.008;

class Utoma {

	constructor (container, canvas_context, board, image_data) {
		this.x = Math.random () * board.width;
		this.y = Math.random () * board.height;
		this.z = Math.random ();
		this.speed = 0.01 + 0.02 * Math.random ();
		this.rotation = 2 * Math.PI * Math.random ();
		this.container = container;
		this.board = board;
		this.sprite = this.create_sprite ();
		this.watcher = null;
		this.canvas_context = canvas_context;
		this.image_data = image_data;
		this.image_scale = container.getBoundingClientRect().height; // FIXME
		this.canvas_scale = canvas_context.canvas.height; // FIXME
		this.min_speed = UTOMA_MIN_SPEED;
		this.max_speed = UTOMA_MAX_SPEED;
		this.paused = false;
		this.init_neurons ();
		this.step_count = 0;
		this.last_mark_step = 0;
		this.min_mark_interval = 100;
		this.stay = false;
	}

	step (force=false) {
		if (!this.paused || force) {
			this.clear_antennas ();
			this.read_inputs();
			this.feedback();
			this.neural.calculate_outputs();
			if (!this.stay) {
				this.update_position();
				this.put_marks();
			}
			if (this.step_count % UTOMA_UPDATE_SPRITE_INTERVAL == 0) {
				this.update_sprite();
			}
			this.step_count++;
		}
	}

	read_inputs () {
		var pix;

		pix = this.get_pixel (0, 0);
		this.neural.inputs[0] = pix[0]; // this.set_input ('EYE_0_R', pix[0]);
		this.neural.inputs[1] = pix[1]; // this.set_input ('EYE_0_G', pix[1]);
		this.neural.inputs[2] = pix[2]; // this.set_input ('EYE_0_B', pix[2]);

		pix = this.get_pixel (0.1, 0);
		this.neural.inputs[3] = pix[0]; // this.set_input ('EYE_1_R', pix[0]);
		this.neural.inputs[4] = pix[1]; // this.set_input ('EYE_1_G', pix[1]);
		this.neural.inputs[5] = pix[2]; // this.set_input ('EYE_1_B', pix[2]);
		
		pix = this.get_pixel (0.2, 0);
		this.neural.inputs[6] = pix[0]; // this.set_input ('EYE_2_R', pix[0]);
		this.neural.inputs[7] = pix[1]; // this.set_input ('EYE_2_G', pix[1]);
		this.neural.inputs[8] = pix[2]; // this.set_input ('EYE_2_B', pix[2]);

		pix = this.get_pixel (-0.05, -0.1);
		this.neural.inputs[9] = pix[0]; // this.set_input ('EYE_3_R', pix[0]);
		this.neural.inputs[10] = pix[1]; // this.set_input ('EYE_3_G', pix[1]);
		this.neural.inputs[11] = pix[2]; // this.set_input ('EYE_3_B', pix[2]);

		pix = this.get_pixel (0.05, -0.1);
		this.neural.inputs[12] = pix[0]; // this.set_input ('EYE_4_R', pix[0]);
		this.neural.inputs[13] = pix[1]; // this.set_input ('EYE_4_G', pix[1]);
		this.neural.inputs[14] = pix[2]; // this.set_input ('EYE_4_B', pix[2]);

		pix = this.get_pixel (0.15, -0.1);
		this.neural.inputs[15] = pix[0]; // this.set_input ('EYE_5_R', pix[0]);
		this.neural.inputs[16] = pix[1]; // this.set_input ('EYE_5_G', pix[1]);
		this.neural.inputs[17] = pix[2]; // this.set_input ('EYE_5_B', pix[2]);

		pix = this.get_pixel (-0.05, 0.1);
		this.neural.inputs[18] = pix[0]; // this.set_input ('EYE_6_R', pix[0]);
		this.neural.inputs[19] = pix[1]; // this.set_input ('EYE_6_G', pix[1]);
		this.neural.inputs[20] = pix[2]; // this.set_input ('EYE_6_B', pix[2]);

		pix = this.get_pixel (0.05, 0.1);
		this.neural.inputs[21] = pix[0]; // this.set_input ('EYE_7_R', pix[0]);
		this.neural.inputs[22] = pix[1]; // this.set_input ('EYE_7_G', pix[1]);
		this.neural.inputs[23] = pix[2]; // this.set_input ('EYE_7_B', pix[2]);

		pix = this.get_pixel (0.15, 0.1);
		this.neural.inputs[24] = pix[0]; // this.set_input ('EYE_8_R', pix[0]);
		this.neural.inputs[25] = pix[1]; // this.set_input ('EYE_8_G', pix[1]);
		this.neural.inputs[26] = pix[2]; // this.set_input ('EYE_8_B', pix[2]);
	}

	feedback () {
		this.set_input ('FEEDBACK_IN_1', this.get_output ('FEEDBACK_OUT_1'));
		this.set_input ('FEEDBACK_IN_2', this.get_output ('FEEDBACK_OUT_2'));
		this.set_input ('FEEDBACK_IN_3', this.get_output ('FEEDBACK_OUT_3'));
		this.set_input ('ALTITUDE', this.z);
		this.set_input ('SPEED', this.speed);

	}

	create_sprite() {
		return this.create_sprite_bee();
	}

	create_sprite_div () {
		var sprite = document.createElement ("DIV");
		sprite.classList.add ("bee");
		sprite.style.background = '#fff';
		sprite.style.width = '70px';
		sprite.style.height = '55px';
		sprite.position = 'absolute';
		this.container.appendChild (sprite);
		if (this.watcher != null) {
			this.sprite.addEventListener ('click', () => this.watcher.watch (this));
		}
		return sprite;
	}

	create_sprite_bee () {
		var sprite = document.createElement ("IMG");
		sprite.classList.add ("bee");
		sprite.src = 'img/abeja.png';
		sprite.width = '70';
		sprite.height = '55';
		sprite.position = 'absolute';
		this.container.appendChild (sprite);
		if (this.watcher != null) {
			this.sprite.addEventListener ('click', () => this.watcher.watch (this));
		}
		return sprite;
	}

	set_watcher (watcher) {
		this.watcher = watcher;
		if (this.sprite != null) {
			this.sprite.addEventListener ('click', () => this.watcher.watch (this));
		}
	}

	update_position () {
		this.rotation += this.get_output ('ROTATION')*UTOMA_ROTATION_SPEED;
		if (this.rotation > Math.PI) {
			this.rotation -= 2*Math.PI;
		} else if (this.rotation < -Math.PI) {
			this.rotation += 2*Math.PI;
		}
		this.speed += this.get_output('SPEED')*UTOMA_ACCELERATION;
		//this.speed = this.min_speed + this.get_output('SPEED') * 1+(this.max_speed - this.min_speed)/2
		if (this.speed < this.min_speed) {
			this.speed = this.min_speed;
		}
		if (this.speed > this.max_speed) {
			this.speed = this.max_speed;
		}
		this.x = this.x + Math.cos (this.rotation-Math.PI/2) * this.speed;
		this.y = this.y + Math.sin (this.rotation-Math.PI/2) * this.speed;
		this.z = this.z + this.get_output ('ALTITUDE')*UTOMA_ACCELERATION;
		if (this.x > this.board.width) {
			this.x = this.board.width - 0.001;
			this.rotation += Math.PI/2;
		} else if (this.x < 0) {
			this.x = 0.001;
			this.rotation += Math.PI/2;
		}
		if (this.y > this.board.height) {
			this.y = this.board.height - 0.001;
			this.rotation += Math.PI/2;
		} else if (this.y < 0) {
			this.y = 0.001;
			this.rotation += Math.PI/2;
		}
		if (this.z < 0) {
			this.z = 0;
		} else if (this.z > 1) {
			this.z = 1;
		}
	}

	put_marks() {
		if (
			((this.step_count - this.last_mark_step) > this.min_mark_interval)
			&&(this.get_output ('MARK_ACTIVE') > 0.95)
		) {
			this.board.add_mark ({
				x: this.x,
				y: this.y,
				amplitude: 1,
				color: this.get_output ('MARK_COLOR')
			});
			this.last_mark_step = this.step_count;
			//this.stay = true;
		}
	}

	update_sprite () {
		this.sprite.style.left = ((this.x*this.image_scale) -35)+"px";
		this.sprite.style.top = ((this.y*this.image_scale) - 27.5)+"px";
		this.sprite.style.transform = 'rotate('+(this.rotation)+'rad) scale('+(0.5+this.z/3)+')';
	}

	clear_antennas () {
		for (let i=0; i<6; i++) {
			this.set_input('ANTENNA_AMPLITUDE_'+i, 0);
			this.set_input('ANTENNA_COLOR_'+i, 0);
		}
	}

	pause () {
		this.paused = true;
	}

	play () {
		this.paused = false;
	}

	get_pixel (offset_forward, offset_right) {
		var sin = Math.sin (this.rotation);
		var cos = Math.cos (this.rotation);
		var offset_x = (sin*offset_forward + cos*offset_right)*(1+this.z)/2;
		var offset_y = (-cos*offset_forward + sin*offset_right)*(1+this.z)/2;
		var target_x = Math.round ((this.x + offset_x) * this.canvas_scale);
		var target_y = Math.round ((this.y + offset_y) * this.canvas_scale);
		//return this.canvas_context.getImageData ((this.x + offset_x) * this.canvas_scale, (this.y + offset_y) * this.canvas_scale, 1, 1).data;
		var retval;
		if (target_x > 0 && target_y > 0 && target_x < this.image_data.width && target_y < this.image_data.height) {
			return this.image_data.pixels[target_x][target_y];
		} else {
			return [0,0,0,0];
		}
		return retval;
	}

	init_neurons () {
		this.input_neuron_names = [ 
			'EYE_0_R', 'EYE_0_G', 'EYE_0_B',
			'EYE_1_R', 'EYE_1_G', 'EYE_1_B',
			'EYE_2_R', 'EYE_2_G', 'EYE_2_B',
			'EYE_3_R', 'EYE_3_G', 'EYE_3_B',
			'EYE_4_R', 'EYE_4_G', 'EYE_4_B',
			'EYE_5_R', 'EYE_5_G', 'EYE_5_B',
			'EYE_6_R', 'EYE_6_G', 'EYE_6_B',
			'EYE_7_R', 'EYE_7_G', 'EYE_7_B',
			'EYE_8_R', 'EYE_8_G', 'EYE_8_B',
			'ANTENNA_COLOR_0', 'ANTENNA_AMPLITUDE_0',
			'ANTENNA_COLOR_1', 'ANTENNA_AMPLITUDE_1',
			'ANTENNA_COLOR_2', 'ANTENNA_AMPLITUDE_2',
			'ANTENNA_COLOR_3', 'ANTENNA_AMPLITUDE_3',
			'ANTENNA_COLOR_4', 'ANTENNA_AMPLITUDE_4',
			'ANTENNA_COLOR_5', 'ANTENNA_AMPLITUDE_5',
			'ALTITUDE',
			'SPEED',
			'FEEDBACK_IN_1', 'FEEDBACK_IN_2', 'FEEDBACK_IN_3'
		]

		this.output_neuron_names = [
			'ROTATION',
			'ALTITUDE',
			'SPEED' ,
			'MARK_COLOR',
			'MARK_ACTIVE',
			'FEEDBACK_OUT_1', 'FEEDBACK_OUT_2', 'FEEDBACK_OUT_3'
		];

		this.neural = new Neural ([this.input_neuron_names.length, 6, this.output_neuron_names.length]);

		this.ineurons = {};
		for (let i = 0; i < this.input_neuron_names.length; i++) {
			this.ineurons [this.input_neuron_names[i]] = i;
		}

		this.oneurons = {};
		for (let i = 0; i < this.output_neuron_names.length; i++) {
			this.oneurons [this.output_neuron_names[i]] = i;
		}
	}

	notify_mark (mark) {
		/*this.sprite.style.backgroundColor = mark.rgb;
		this.sprite.style.borderRadius = "50%";
		window.setTimeout (() => {
			this.sprite.style.backgroundColor = '';
			this.sprite.style.borderRadius = 0;
		}, 100)
		*/
		var rel_x = mark.x - this.x;
		var rel_y = mark.y - this.y;
		var distance = Math.sqrt (Math.pow (rel_x, 2) + Math.pow (rel_y, 2));
		var active_antenna = 0;
		var angle = Math.atan2 (rel_y, rel_x) - this.rotation+Math.PI*5/6; //FIXME: REVISAR SI ANGULO ES EXACTO
		while (angle > 2*Math.PI) {
			angle -= 2*Math.PI;
		}
		while (angle < 0) {
			angle += 2*Math.PI;
		}
		var active_antenna = Math.floor (angle*3/Math.PI);
		this.set_input ("ANTENNA_COLOR_"+active_antenna, mark.color);
		this.set_input ("ANTENNA_AMPLITUDE_"+active_antenna, 1-distance);
		if ((this.watcher != null) && (this.watcher.utoma == this)) {
			this.watcher.notify_antenna (active_antenna, mark.color, 1-distance);
		}
	}

	set_input (name, value) {
		this.neural.inputs[this.ineurons[name]] = value;
	}

	get_input (name) {
		return this.neural.inputs[this.ineurons[name]];
	}

	set_output (name, value) {
		this.neural.outputs[this.oneurons[name]] = value;
	}

	get_output (name) {
		return this.neural.outputs[this.oneurons[name]];
	}

}
