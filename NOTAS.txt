GLOBAL:
	(+) Respawn

ALGORITMO GENETICO:
	(+) Cruzamiento
	(+) Mutacion condicional
	(+) Ruleta cruzamiento
	(+) Ruleta desaparición

RED NEURONAL:
	(+) Función de activación
	(+) Bias
	(+) Capa oculta
	( ) Aumentar probabilidad de comunicación
	(+) Generar genoma
	(+) Memoria

INTERFAZ:
	( ) Parametros configurables
	( ) Tamaños se ajusten despues de redimensionar pantalla
	( ) Seleccionar y deseleccionar abeja
	( ) Boton FF
	( ) Estilo CSS
